package com.easemob.pubsubexamples;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;

public class SubscribeExample {

	public static void main(String[] argv) throws Exception {
        //"guest"为测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
		//客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
		//建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
		String clientUUID = "123456789";

	    //"testchannel1"为测试用消息频道名
		easeMob.subscribe("testchannel1", clientUUID, new EaseMobCallBack() {
			@Override
			public void onMessage(Object message) {
				Date currentTime = new Date();
		        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
				String currentTimeString = sdf.format(currentTime);
				System.out.println(" [x] Received on " + currentTimeString + " : " + message);
			}

			@Override
			public void onConnected() {
                System.out.println(" [x] onConnected");    
			}

			@Override
			public void onDisConnected() {
                System.out.println(" [x] onDisConnected");    
			}

			@Override
			public void onReConnected() {
			    System.out.println(" [x] onReConnected");				
			}

            @Override
            public void onReConnecting() {
                System.out.println(" [x] onReConnecting");             
            }

		});
	}
}
