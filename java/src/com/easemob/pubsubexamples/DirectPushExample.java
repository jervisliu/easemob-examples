package com.easemob.pubsubexamples;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;

public class DirectPushExample {

	public static void main(String[] argv) throws Exception {
        //"guest"为公用的测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
		String message = "你好, 推送消息发送自EaseMob。";
		Date currentTime = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
		String currentTimeString = sdf.format(currentTime);
		
	    //"testchannel1"为测试用消息频道名
		easeMob.push("123456788", (message + "发送时间：" + currentTimeString).getBytes());
		System.out.println(" [x] Sent '" + message + "'");
	}

}
