package com.easemob.messagebox.settings;


import com.easemob.messagebox.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ManageAccountActivity extends Activity {

    private Button loginButton;
    private Button registButton;
    private AutoCompleteTextView loginUserNameTextView;
    private EditText pwdEditText;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_account);
        
        loginButton = (Button) findViewById(R.id.bnLogin);
        registButton = (Button) findViewById(R.id.bnRegist);
        loginUserNameTextView = (AutoCompleteTextView)findViewById(R.id.etLoginUsername);
        pwdEditText = (EditText)findViewById(R.id.etPwd);
        
        loginButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                
            }            
        });

        registButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                
            }            
        });
    }
}