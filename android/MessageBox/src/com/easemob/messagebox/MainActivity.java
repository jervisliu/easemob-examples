package com.easemob.messagebox;

import com.easemob.messagebox.settings.ManageChannelActivity;
import com.easemob.messagebox.settings.NoticeSettingActivity;
import com.easemob.messagebox.settings.ViewAccountActivity;

import android.os.Bundle;
import android.os.IBinder;
import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends ListActivity {
	MessageListAdapter adapter = null;
	private PushMessageService pushMessageService;
	  
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        adapter = new MessageListAdapter(this);
        setListAdapter(adapter);
        doStartService();
        doBindService();
        registerReceiver(broadcastReceiver, new IntentFilter(PushMessageService.BROADCAST_ACTION));
    }
    
    @Override
    public void onDestroy () {
        super.onDestroy();
        if (mConnection != null) {
            unbindService(mConnection);
            mConnection = null;
        }
        if(broadcastReceiver != null) {
            unregisterReceiver(broadcastReceiver);
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }   
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:     
                Intent noticeSettingIntent = new Intent(this, NoticeSettingActivity.class);
                startActivity(noticeSettingIntent);
                break;
            case R.id.menu_account:     
                Intent accountIntent = new Intent(this, ViewAccountActivity.class);
                startActivity(accountIntent);
                break;
            case R.id.menu_channel:     
                Intent channelIntent = new Intent(this, ManageChannelActivity.class);
                startActivity(channelIntent);
                break;
          }
        return true;
    }
    
	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder binder) {
			pushMessageService = ((PushMessageService.PushMessageServiceBinder) binder)
					.getService();
			//sync data with the PushMessageService
	        adapter.setEntry(pushMessageService.getPushMessageList());
		}

		public void onServiceDisconnected(ComponentName className) {
			pushMessageService = null;
		}
	};

	void doBindService() {
		bindService(new Intent(this, PushMessageService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}
	
	void doStartService() {
		startService(new Intent(this, PushMessageService.class));
	}
  
    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {        	
			if (pushMessageService != null) {
				adapter.setEntry(pushMessageService.getPushMessageList());
			}
        }
    };    
}
