package com.easemob.messagebox;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageListAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private List<PushMessage> mEntries = new ArrayList<PushMessage>();
	
	public MessageListAdapter(Context context) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return mEntries.size();
	}

	@Override
	public Object getItem(int position) {
		return mEntries.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView,	ViewGroup parent) {
        View vi = convertView;
        if(convertView==null) {
            vi = mLayoutInflater.inflate(R.layout.message_list_row, null);
        }
        
        TextView sender = (TextView)vi.findViewById(R.id.sender); // title
        TextView messageDetail = (TextView)vi.findViewById(R.id.messageDetail); // message detail
        TextView sentTime = (TextView)vi.findViewById(R.id.sentTime); // time
        ImageView thumbimage = (ImageView)vi.findViewById(R.id.list_image); // thumb image
 
 
        PushMessage message = mEntries.get(position);
        // Setting all values in listview
        sender.setText(message.getSender());
        messageDetail.setText(message.getMessageDetail());

		Date time = message.getSentTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
		String currentTimeString = sdf.format(time);		
        sentTime.setText(currentTimeString);
        return vi;
	}
	
	public void setEntry(List<PushMessage> entry) {
		mEntries = entry;		
		notifyDataSetChanged();
	}

}
