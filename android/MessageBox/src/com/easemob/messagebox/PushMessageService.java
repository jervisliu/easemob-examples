package com.easemob.messagebox;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;

public class PushMessageService extends Service {
	public static final String BROADCAST_ACTION = "com.easemob.messagebox.on_receive_pushmessage";

	private final IBinder mBinder = new PushMessageServiceBinder();
	private ArrayList<PushMessage> pushMessageList = new ArrayList<PushMessage>();

	Intent broadcaseIntent;
	EaseMob easeMob;
	
    @Override
    public void onCreate() {
		super.onCreate();	
		broadcaseIntent = new Intent(BROADCAST_ACTION);
		EaseMobSubscriberTask task = new EaseMobSubscriberTask();
	    task.execute();
 
  /*		
		PushMessage pushMessage = new PushMessage();
		pushMessage.setMessageDetail("测试消息1");
		pushMessage.setSender("差评修复大师");
		pushMessage.setSentTime(new Date());		
		pushMessageList.add(pushMessage);
		sendBroadcast(broadcaseIntent);*/
	}

    @Override
	public int onStartCommand(Intent intent, int flags, int startId) {    	
		return Service.START_STICKY;
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return mBinder;
	}

	public class PushMessageServiceBinder extends Binder {
		PushMessageService getService() {
			return PushMessageService.this;
		}
	}

	public List<PushMessage> getPushMessageList() {
		return pushMessageList;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		//TODO:
		//easeMob.disconnect();
	}
    
    private class EaseMobSubscriberTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... v) {            
            //configuration has not been initialized yet. Set to default values
            String savedChannelName = Gl.getSubscribedChannel();
            if(savedChannelName == null) {
                Gl.setSubscribedChannel("testchannel1");
                Gl.setChannleUserName("guest");       
                Gl.setChannlePwd("guest");
            }

            //对于不需要对指定客户端进行单点推送的应用可以使用由系统产生的UUID，该UUID在APP第一次安装时产生 。
            //对于需要对指定客户端进行单点推送的应用可以由服务器根据长UUID自动产生一个短UUID，用户手动输入该短UUID
            final String clientUUID = Gl.getDeviceUUID();
            
            //"testchannel1"为公用的测试消息频道
            final String channelName = Gl.getSubscribedChannel();
            //"guest"为公用的测试用户名,密码为"guest"
            String userName = Gl.getChannleUserName();
            String pwd = Gl.getChannlePwd();
            
            easeMob = new EaseMobImpl(userName, pwd);

            easeMob.subscribe(channelName, clientUUID, new EaseMobCallBack() {
                @Override
                public void onMessage(final Object message) {
                    System.out.println(" [x] Received '" + message + "'");
                    PushMessage pushMessage = new PushMessage();
                    pushMessage.setMessageDetail((String)message);
                    pushMessage.setSender("差评修复大师");
                    pushMessage.setSentTime(new Date());        
                    pushMessageList.add(pushMessage);

                    sendBroadcast(broadcaseIntent);
                    sendNotification(pushMessage);
                }

                @Override
                public void onConnected() {
                    System.out.println(" [x] onConnected '");         
                    System.out.println(" [x] Subscribed to channel " + channelName + ". Client Id is " + clientUUID);
                }

                @Override
                public void onDisConnected() {
                    System.out.println(" [x] onDisConnected '");   
                    String cserviceName = Context.CONNECTIVITY_SERVICE;  
                    ConnectivityManager cm = (ConnectivityManager) getSystemService(cserviceName);  
                    //没有网络连接时不要盲目的自动重连
                    if (cm.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED) { 
                        easeMob.setAutoReconnect(false);
                        easeMob.closeConnection();
                        easeMob = null;
                        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);        
                        registerReceiver(connectivityBroadcastReceiver, filter);
                        System.out.println(" [x] Stopped reconnecting as there is no network.  '");   
                    }
                }

                @Override
                public void onReConnected() {
                    System.out.println(" [x] onReConnected '");   
                    easeMob.setAutoReconnect(true);                
                }

                @Override
                public void onReConnecting() {
                    System.out.println(" [x] onReConnecting '");    
                }

            });
          
            return null;
        }
    }       
	
	public static void sendNotification(PushMessage pushMessage) {
		if (Gl.getAlertNotificationEnable()) {
			NotificationManager manager = (NotificationManager) Gl.Ct().getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(R.drawable.ic_launcher, pushMessage.getMessageDetail(), System.currentTimeMillis());

			if (Gl.getNoticedBySound()) {
				notification.defaults |= Notification.DEFAULT_SOUND;
			}
			if (Gl.getNoticedByVibrate()) {
				notification.defaults |= Notification.DEFAULT_VIBRATE;
			}
			notification.flags |= Notification.FLAG_AUTO_CANCEL;

			Intent notifyIntent = new Intent(Gl.Ct(), MainActivity.class);
			notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			PendingIntent contentIntent = PendingIntent.getActivity(Gl.Ct(), notification.hashCode(), notifyIntent, 0);
			notification.setLatestEventInfo(Gl.Ct(), pushMessage.getSender(), pushMessage.getMessageDetail(), contentIntent);
			manager.notify(notification.hashCode(), notification);
		}
	}
	
	private BroadcastReceiver connectivityBroadcastReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	        boolean noConnectivity = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);
	        if(!noConnectivity) {
	            unregisterReceiver(connectivityBroadcastReceiver);
	            EaseMobSubscriberTask task = new EaseMobSubscriberTask();
	            task.execute();
	        }
	    }
	};
}