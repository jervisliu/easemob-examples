package com.easemob.subscribeexample;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.easemob.pubsub.EaseMob;
import com.easemob.pubsub.EaseMobCallBack;
import com.easemob.pubsub.impl.rabbitmq.EaseMobImpl;


import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {
	private TextView mOutput;
	EaseMob easeMob = null;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.activity_main);

        mOutput =  (TextView) findViewById(R.id.output);
        
        //"guest"为公用的测试用户名,密码为"guest"
        easeMob = new EaseMobImpl("guest", "guest");
        
        //客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
        //建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
        String clientUUID = "123456788";

        //"testchannel1"为测试用消息频道名
        easeMob.subscribe("testchannel1", clientUUID, new EaseMobCallBack() {
			@Override
			public void onMessage(final Object message) {
				System.out.println(" [x] Received '" + message + "'");
				
				mOutput.post(new Runnable() {
	                public void run() {
	    				Date currentTime = new Date();
	    		        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS"); 
	    		        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
	    				String currentTimeString = sdf.format(currentTime);

	                	mOutput.append("\n"+" Received on " + currentTimeString + " : " + message);
	                }
	            });
			}

            @Override
            public void onConnected() {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void onDisConnected() {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void onReConnected() {
                // TODO Auto-generated method stub
                
            }

            @Override
            public void onReConnecting() {
                // TODO Auto-generated method stub
                
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
	@Override
	public void onDestroy() {
		super.onDestroy();

		//TODO:
		//easeMob.disconnect();
	}
}
