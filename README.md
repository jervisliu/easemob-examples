Java Subscribe/Publish Example
==============================

1. 注册EaseMob帐户

登陆www.easemob.com，注册帐户。


2. 创建消息频道

每个EaseMob帐户可创建一个到多个消息频道。消息频道名称由系统自动产生，命名规则为：EaseMob帐户名-消息频道UUID


3. 安全管理

3.1 为避免消息频道被他人侦听或滥用，消息频道的消息接收和消息发送使用2套不同的用户名/密码。在EaseMob的用户管理下可找到消息接收和消息发送对应的用户名/密码

3.2 使用SSL：TODO


4.接收消息.
EaseMob的消息广播机制可以保证每个客户端都一定可以收到推送的消息，并且同一个消息只会收到一次。如果由于各种原因，某个客户端没有收到消息，比如该客户端没有开启，或者该客户端出现临时的网络连接中断，该客户端都会在重新
与EaseMob的服务器建立连接后收到服务器补发的漏收消息。

4.1. 创建一个EaseMob对象：
		//"guest"为公用的测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");

这里你需要使用你在EaseMob帐户下创建的消息接收的用户名/密码和创建的消息频道名称。为方便测试，EaseMob缺省开通了一个公共的测试消息频道，名为"testchannel"。及测试用的公用用户"guest",密码"guest".


4.2 订阅要接收消息的消息频道，接收消息：
订阅消息频道时，每个客户端都需要提供一个唯一的客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID。

		//客户端UUID。EaseMob需要通过该UUID来记录那些客户端收到了推送消息，哪些客户端还没有收到推送消息。
		//建议使用java.util.UUID类或者通过Android DeviceID来产生该客户端UUID，或者使用客户端APP的注册帐户名
        String clientUUID = "123456789";
        //"testchannel1"为测试用消息频道名
		easeMob.subscribe("testchannel1", clientUUID, new EaseMobCallBack() {
			@Override
			public void onMessage(Object message) {
				System.out.println(" [x] Received '" + message + "'");
			}

		});
		

5.广播式推送消息

5.1 创建一个EaseMob对象：
		//"guest"为公用的测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
这里你需要使用你在EaseMob帐户下创建的消息发送的用户名/密码。为方便测试，EaseMob缺省开通了一个测试用的公用用户"guest",密码"guest".

5.2 向指定的消息频道推送消息：
	
		String message = "hello";
		easeMob.publish("testchannel", message);
		
为方便测试，EaseMob缺省开通了一个公共的测试消息频道，名为"testchannel"。发送的消息类型为String,也可以使用json等其他类型。


6. 点对点推送消息

6.1 创建一个EaseMob对象：
		//"guest"为公用的测试用户名,密码为"guest"
		EaseMob easeMob = new EaseMobImpl("guest", "guest");
		
这里你需要使用你在EaseMob帐户下创建的消息发送的用户名/密码。为方便测试，EaseMob缺省开通了一个测试用的公用用户"guest",密码"guest".

6.2 向指定的客户端推送消息：
	
		String message = "hello";
		easeMob.push("123456789", message);
		
"123456789"为指定的客户端的UUID。发送的消息类型为String,也可以使用json等其他类型。


7. Handling network connectivity errors and disconnects

The nature of a mobile device is that connections will come and go. There are a number of things you can 
do to ensure that your EaseMob connection remains active for as long as you have a network connection and 
reconnects after network connectivity has been re-established.

7.1 自动重连
你可以设置EaseMob的自动重连和自动重连延迟属性，当初始连接未能成功或成功连接后网络连接出现中断，EaseMob都会进行自动重连。

EaseMob easeMob = new EaseMobImpl();
easeMob.setReconnectAutomatically(true); //defaults to true
easeMob.setReconnectDelay(30); // defaults to 5 seconds

注：自动重连缺省为打开，自动重连延时缺省为5秒钟

7.2 EaseMob与网络连接有关的回调方法：
EaseMob会在网络连接出现问题时自动重连并会调用以下方法通知客户端:
public interface EaseMobCallBack {
    public void onMessage(Object message); //收到消息时会调用此方法	
	public void onConnected(); //第一次建立网络连接时会调用此方法	
	public void onDisConnected();//网络连接中断或出现异常时会调用此方法
	public void onReConnected();//网络连接恢复时会调用此方法
	public void onReConnecting();//尝试自动重连时会调用此方法
}
	
7.3 使用Android ConnectivityManager监控网络连接状态，管理自动重连
为避免没有网络连接时盲目的自动重连，你可以使用Android ConnectivityManager监控网络连接状态，当没有网络连接时暂时关闭自动重连。

            @Override
            public void onDisConnected() {
                String cserviceName = Context.CONNECTIVITY_SERVICE;  
                ConnectivityManager cm = (ConnectivityManager) getSystemService(cserviceName);  
                //没有网络连接时不要盲目的自动重连
                if (cm.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED) { 
                    easeMob.setAutoReconnect(false);
                }
            }

接收ConnectivityManager发送的消息，等待网络状态变为可连接。当然这不意味你一定可以重新和EaseMob建立连接，但至少这是一个合理的信号表明你可以重新尝试重连了。

TODO:



注:使用Android ConnectivityManager需要在AndroidManifest.xml文件中指定访问许可：
    <uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>  
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE"/>  
    
7.4 网络重连成功后重新打开自动重连

            @Override
            public void onReConnected() {
                easeMob.setAutoReconnect(true);                
            }
            
7.5 使用onReConnecting()对自动重连进行计数
你还可以用onReConnecting()对自动重连进行计数。当自动重连失败次数超过一定数量时，你可以增长自动重连的延时时间。
	
	
8. OnPresence: TODO

9. History: TODO
